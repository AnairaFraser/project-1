# PROJECT 1

## Description

An online reimbursement system powered by Java servlets.

## Technology Used

* Java 8
  * OOP
  * JDBC/DAO
  * Maven
  * Servlets
* HTML
* CSS
* JavaScript
* Bootstrap
* SQL (Oracle)
* AWS
* DBeaver
* Apache Tomcat v9
* Web Browswer


## Features

#### Available Features
* Users 
  * Creating accounts (employees or financial managers)
  * Logging in/logging out of accounts
  * Employees and submit reimbursement requests and view their requests
  * Financial managers can approve/deny reimbursement requests and view all reimbursement tickets
* System
  * Verify user sessions
  * Process requests from the websites and send back responses

#### To-Do List

* Allow user to edit their credentials

## Getting Started/Usage

* Downloading Project:
```
git clone https://gitlab.com/AnairaFraser/project-1/
```
* Must have [DBeaver](https://dbeaver.io) and [Apache Tomcat 9](https://tomcat.apache.org/download-90.cgi) installed in order to interact with project
* Create a new Oracle connection in DBeaver using the following credentials: (do not forget to disconnect when finished using project)
  * Username: admin
  * Password: foobarre
  * URL: jdbc:oracle:thin:@database-1.cdaukxcx8a8t.us-east-2.rds.amazonaws.com:1521:ORCL
* Run the Maven project on Apache Tomcat, then navigate to the URL http://localhost:8080/ProjectOne/login.html to begin
